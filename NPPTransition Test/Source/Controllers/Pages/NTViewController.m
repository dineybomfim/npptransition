/*
 *	NTViewController.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NTViewController.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

static void ntNextTransition(void)
{
	static int i = 0;
	Class nextClass = NULL;
	
	switch (i)
	{
		case 0:
			nextClass = [NPPTransitionScale class];
			break;
		case 1:
			nextClass = [NPPTransitionEasing class];
			break;
		case 2:
			nextClass = [NPPTransitionFade class];
			break;
		case 3:
			nextClass = [NPPTransitionFlip class];
			break;
		case 4:
			nextClass = [NPPTransitionFold class];
			break;
		case 5:
			nextClass = [NPPTransitionRoom class];
			break;
		case 6:
			nextClass = [NPPTransitionSlide class];
			break;
		case 7:
			nextClass = [NPPTransitionTurn class];
			break;
		case 8:
		default:
			nextClass = [NPPTransitionCube class];
			break;
	}
	
	[NPPTransition definePushTransitionClass:nextClass direction:NPPDirectionLeft];
	
	i = (i >= 8) ? 0 : i + 1;
}

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NTViewController

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

@synthesize btNext = _btNext, btModal = _btModal, btNotification = _btNotification;

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (IBAction) actionNext:(id)sender
{
	Class selfClass = [self class];
	UIViewController *next = [[selfClass alloc] init];
	[[self navigationController] pushViewController:next animated:YES];
	nppRelease(next);
}

- (IBAction) actionModal:(id)sender
{
	UIStoryboard *scene = [UIStoryboard storyboardWithName:@"ModalStoryboard" bundle:nil];
	UIViewController *modal = [scene instantiateViewControllerWithIdentifier:@"NTModalViewControllerID"];
	[[self navigationController] presentViewController:modal animated:YES completion:nil];
}

- (IBAction) actionNotification:(id)sender
{
	// It's not the intention here to make a generice reusable notification class,
	// this is just to show you how easy could be to make transitions between views.
	NTNotificationView *notification = [NTNotificationView notificationWithType:NTNotificationTypeiOS6];
	notification.titleText = @"Notification Title";
	notification.subText = @"This is the notification text...";
	
	// Small trick to avoid being behind the App status bar,
	// as is not the intention here to cover the status bar.
	CGRect frame = notification.frame;
	CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
	CGRect frameInWindow = [self.view.window convertRect:statusBarFrame fromWindow:nil];
	CGRect frameInView = [self.view convertRect:frameInWindow fromView:nil];
	frame.origin.y += frameInView.size.height;
	notification.frame = frame;
	
	// As this is just an example, we'll not create a real notification API,
	// which should overriding the application status bar.
	// So, let's place our animation directly in the navigation bar.
	UIView *fromView = self.navigationController.navigationBar;
	
	// Now our custom notification is ready.
	
	//*************************
	//	Transition Stuff
	//*************************
	
	// Creating a custom transition.
	NPPTransitionCube *cube = [NPPTransitionCube transitionFromView:fromView
															 toView:notification
														 completion:nil];
	cube.direction = NPPDirectionDown;
	cube.mode = NPPTransitionModeOverride;
	[cube perform];
	
	// Preparing it to goes back after 2 seconds.
	[cube performBackwardAfterDelay:2.0f];
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) viewDidLoad
{
	[super viewDidLoad];
	
	// Chooses the next view color.
	self.view.backgroundColor = [UIColor nextViewColor];
	
	CALayer *layer;
	layer = _btNext.layer;
	layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
	layer.shadowOpacity = 0.15f;
	layer.shadowRadius = 0.0f;
	layer.shadowColor = [[UIColor blackColor] CGColor];
	
	layer = _btModal.layer;
	layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
	layer.shadowOpacity = 0.15f;
	layer.shadowRadius = 0.0f;
	layer.shadowColor = [[UIColor blackColor] CGColor];
	
	layer = _btNotification.layer;
	layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
	layer.shadowOpacity = 0.15f;
	layer.shadowRadius = 0.0f;
	layer.shadowColor = [[UIColor blackColor] CGColor];
}


- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	// TODO how to deal with the calls in the routine ???
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// Prepares the next transition type.
	ntNextTransition();
}

- (void) dealloc
{
#ifndef NPP_ARC
	[super dealloc];
#endif
}

@end
