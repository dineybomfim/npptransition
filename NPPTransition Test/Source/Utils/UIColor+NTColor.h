/*
 *	UIColor+NTColor.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

@interface UIColor(NTColor)

+ (UIColor *) colorWithHexadecimal:(unsigned int)hex;
+ (UIColor *) nextViewColor;

@end