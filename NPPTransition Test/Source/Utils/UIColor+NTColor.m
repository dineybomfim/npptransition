/*
 *	UIColor+NTColor.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "UIColor+NTColor.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#define kColorsCount		5

static unsigned int _colors[kColorsCount] = { 0x0078c8, 0x1ab853, 0xfdb33f, 0xfd493f, 0xf474ac };

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Categories
#pragma mark -
//**********************************************************************************************************
//
//	Categories
//
//**********************************************************************************************************

@implementation UIColor(NTColor)

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

+ (UIColor *) colorWithHexadecimal:(unsigned int)hex
{
	CGFloat red = ((hex >> 16) & 0xFF) / 255.0f;
	CGFloat green = ((hex >> 8) & 0xFF) / 255.0f;
	CGFloat blue = ((hex >> 0) & 0xFF) / 255.0f;
	CGFloat alpha = 1.0f;
	
	return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (UIColor *) nextViewColor
{
	static unsigned int i = 0;
	i = (i >= kColorsCount) ? 0 : i;
	return [UIColor colorWithHexadecimal:_colors[i++]];
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

@end
