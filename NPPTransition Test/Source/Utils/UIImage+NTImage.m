/*
 *	UIImage+NTImage.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/9/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "UIImage+NTImage.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation UIImage(NTImage)

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (UIImage *) imageNineSliced
{
	CGSize size = self.size;
	
	int width = (int)(size.width + 1) >> 1;
	int height = (int)(size.height + 1) >> 1;
	
	UIEdgeInsets edge = UIEdgeInsetsMake(height - 1, width - 1, height + 1, width + 1);
	
	return [self resizableImageWithCapInsets:edge];
}

- (UIImage *) imageCroppedInRect:(CGRect)rect
{
	UIImageOrientation orientation = self.imageOrientation;
	float scale = self.scale;
	rect.origin.x *= scale;
	rect.origin.y *= scale;
	rect.size.width *= scale;
	rect.size.height *= scale;
	
	CGImageRef crop = CGImageCreateWithImageInRect([self CGImage], rect);
	UIImage *newImage = [UIImage imageWithCGImage:crop scale:scale orientation:orientation];
	CGImageRelease(crop);
	
	return newImage;
}

- (UIImage *) imageRotatedBy:(float)degrees
{
	CGImageRef cgImage = [self CGImage];
	CGSize size =  self.size;
	
	// Sets the CoreGraphic Image to start work on it.
	UIGraphicsBeginImageContext(size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// Unflip the CGImage, a small workaround as CGImages are upside-down.
	CGContextTranslateCTM(context, 0.0f, size.height);
	CGContextScaleCTM(context, 1.0f, -1.0f);
	
	// Rotating the image by its central position.
	CGContextTranslateCTM(context, size.width * 0.5f, size.height * 0.5f);
	CGContextRotateCTM(context, NPPDegreesToRadians(degrees));
	CGContextTranslateCTM(context, -size.width * 0.5f, -size.height * 0.5f);
	
	// Clears and Draws the image in the context to further use the pixel data inside it.
	CGContextClearRect(context, CGRectMake(0.0f, 0.0f, size.width, size.height));
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), cgImage);
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

- (UIImage *) imageScaledX:(float)scaleX andY:(float)scaleY
{
	CGImageRef cgImage = [self CGImage];
	CGSize size =  self.size;
	
	// Sets the CoreGraphic Image to start work on it.
	UIGraphicsBeginImageContext(size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	// Unflip the CGImage, a small workaround as CGImages are upside-down.
	CGContextTranslateCTM(context, 0.0f, size.height);
	CGContextScaleCTM(context, 1.0f, -1.0f);
	
	CGContextTranslateCTM(context, size.width * 0.5f, size.height * 0.5f);
	CGContextScaleCTM(context, scaleX, scaleY);
	CGContextTranslateCTM(context, -size.width * 0.5f, -size.height * 0.5f);
	
	// Clears and Draws the image in the context to further use the pixel data inside it.
	CGContextClearRect(context, CGRectMake(0.0f, 0.0f, size.width, size.height));
	CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), cgImage);
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

@end
