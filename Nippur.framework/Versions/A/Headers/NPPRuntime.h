/*
 *	NPPRuntime.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 7/24/12.
 *	Copyright 2012 db-in. All rights reserved.
 */

#import <objc/objc.h>
#import <objc/message.h>
#import <objc/runtime.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>

#pragma mark -
#pragma mark Basic Definitions
#pragma mark -
//**********************************************************************************************************
//
//	Basic Definitions
//
//**********************************************************************************************************

#pragma mark -
#pragma mark NPP Definitions
//**************************************************
//	NPP Definitions
//**************************************************

// NinevehGL Current Version.
#define NPP_VERSION					1.0f

#pragma mark -
#pragma mark iOS Definitions
//**************************************************
//	iOS Definitions
//**************************************************

// Checks for iOS 4 or above.
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_4_0

	// Prevents compiling for iOS 3.x or earlier.
	#error This project only works with iOS 4.0 and later.

#endif

#pragma mark -
#pragma mark Compiler Definitions
//**************************************************
//	Compiler Definitions
//**************************************************

// Defines the static functions. The Inline instructions is a little bit more expensive.
#define NPP_INLINE					static inline

// Defines the debug mode for the simulator.
#if TARGET_IPHONE_SIMULATOR

	// Enables the debug mode only in the simulator.
	#define NPP_DEBUG

#endif

// Defines the ARC instructions, ONLY FOR PUBLIC HEADERS.
#if __has_feature(objc_arc)

	// ARC definition.
	#define NPP_ARC

	// Convertion instructions.
	#define NPP_ARC_UNSAFE			__unsafe_unretained
	#define NPP_ARC_BRIDGE			__bridge
	#define NPP_ARC_ASSIGN			__weak
	#define NPP_ARC_RETAIN			__strong

	// Property definitions
	#define NPP_RETAIN				strong
	#define NPP_ASSIGN				weak
	#define NPP_COPY				copy

#else

	// Convertion instructions.
	#define NPP_ARC_UNSAFE
	#define NPP_ARC_BRIDGE
	#define NPP_ARC_ASSIGN
	#define NPP_ARC_RETAIN

	// Property definitions
	#define NPP_RETAIN				retain
	#define NPP_ASSIGN				assign
	#define NPP_COPY				copy

#endif

#pragma mark -
#pragma mark C/C++ Definitions
//**************************************************
//	C/C++ Definitions
//**************************************************

// Defines the C/C++ extern patterns
#ifdef __cplusplus

	// Extern instruction for C++ code.
	#define NPP_API					extern "C" __attribute__((visibility ("default")))

#else

	// Extern instruction for C code.
	#define NPP_API					extern __attribute__((visibility ("default")))

#endif

#pragma mark -
#pragma mark OS Versions
//**************************************************
//	OS Versions
//**************************************************

// These constants are to be used in runtime with the function nppDeviceSystemVersion().
// This iOS version is necessary to use:
//	- GCD;
//	- Blocks;
//	- NSRegularExpression.
#define NPP_IOS_4_0					4.0f
#define NPP_IOS_4_1					4.1f
#define NPP_IOS_4_2					4.2f
#define NPP_IOS_4_3					4.3f

// This iOS version is necessary to use:
//	- 32 bits data type in shaders;
//	- Native Twitter account and API.
#define NPP_IOS_5_0					5.0f
#define NPP_IOS_5_1					5.1f

// This iOS version is necessary to use:
//	- Native Facebook account and API.
#define NPP_IOS_6_0					6.0f

// This iOS version is necessary to use:
//	- SpriteKit.
//	- Custom Controller Transitions.
//	- Text Kit.
#define NPP_IOS_7_0					7.0f

#pragma mark -
#pragma mark Geometry
//**************************************************
//	Geometry
//**************************************************

// PI related stuff.
#define kNPP_PI						3.141592f // PI
#define kNPP_2PI					6.283184f // 2 * PI
#define kNPP_PI2					1.570796f // PI / 2
#define kNPP_PI180					0.017453f // PI / 180
#define kNPP_180PI					57.295780f // 180 / PI
#define NPPDegreesToRadians(x)		((x) * kNPP_PI180)
#define NPPRadiansToDegrees(x)		((x) * kNPP_180PI)

#pragma mark -
#pragma mark Functions Definitions
#pragma mark -
//**********************************************************************************************************
//
//	Functions Definitions
//
//**********************************************************************************************************

/*!
 *					Releases a C memory pointer.
 *
 *					Prevents NULL pointers or non-allocated values of being freed (zombies).
 *
 *	@param			x
 *					Any C pointer.
 */
#define nppFree(x)					({ if(x) { free(x); (x) = NULL; } })

/*!
 *					Releases an Objective-C instance.
 *
 *					Prevents nil pointers or non-allocated objects of being released (zombies).
 *
 *	@param			x
 *					Any Obj-C object.
 */
#ifdef NPP_ARC
	#define nppRelease(x)			({ (x) = nil; })
#else
	#define nppRelease(x)			({ if(x) { [x release]; (x) = nil; } })
#endif

/*!
 *					Autoreleases an Objective-C instance.
 *
 *	@param			x
 *					Any Obj-C object.
 */
#ifdef NPP_ARC
	#define nppAutorelease(x)		(x)
#else
	#define nppAutorelease(x)		([x autorelease])
#endif

/*!
 *					Retains an Objective-C instance.
 *
 *	@param			x
 *					Any Obj-C object.
 */
#ifdef NPP_ARC
	#define nppRetain(x)			(x)
#else
	#define nppRetain(x)			([x retain])
#endif

/*!
 *					Safe way to call a block, avoiding nil or invalid instance. This function doesn't
 *					make a copy of the block, it just executes it safely.
 */
#define nppBlock(block, ...)		({ if ((block) != nil) { (block)(__VA_ARGS__); } })