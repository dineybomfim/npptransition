/*
 *	NPPFunctions.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 8/15/12.
 *	Copyright 2012 db-in. All rights reserved.
 */

#import "NPPFunctions.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Public Functions
//**************************************************
//	Public Functions
//**************************************************

float nppDeviceSystemVersion(void)
{
	static float osVersion = 0;
	
	if (osVersion == 0)
	{
		osVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
	}
	
	return osVersion;
}

void nppSwizzle(Class aClass, SEL older, SEL newer)
{
    Method oldSel = class_getInstanceMethod(aClass, older);
    Method newSel = class_getInstanceMethod(aClass, newer);
    if(class_addMethod(aClass, older, method_getImplementation(newSel), method_getTypeEncoding(newSel)))
	{
		class_replaceMethod(aClass, newer, method_getImplementation(oldSel), method_getTypeEncoding(oldSel));
	}
    else
	{
		method_exchangeImplementations(oldSel, newSel);
	}
}