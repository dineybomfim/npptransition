/*
 *	NPPView+UIView.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 9/27/11.
 *	Copyright 2011 db-in. All rights reserved.
 */

#import "NPPView+UIView.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

#pragma mark -
#pragma mark UIView Category
#pragma mark -
//**********************************************************************************************************
//
//	UIView Categories
//
//**********************************************************************************************************

@implementation UIView(NPPView)

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

//*************************
//	Graphics
//*************************

- (UIImage *) snapshot
{
	return [UIView snapshotForView:self inRect:self.bounds];
}

- (UIImage *) snapshotInRect:(CGRect)rect
{
	return [UIView snapshotForView:self inRect:rect];
}

+ (UIImage *) snapshotForView:(UIView *)view inRect:(CGRect)rect
{
	// Create a new render context of the UIView size
	// and set a scale so that the full stage will render to it.
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextTranslateCTM(context, -rect.origin.x, -rect.origin.y);
	
	// Render the stage to the new context.
	[view.layer renderInContext:context];
	
	// Get an image from the context.
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	
	// Finishes the render context.
	UIGraphicsEndImageContext();
	
	return viewImage;
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

@end

#pragma mark -
#pragma mark UIImage Category
#pragma mark -
//**********************************************************************************************************
//
//	UIImage Categories
//
//**********************************************************************************************************

@implementation CALayer(NPPLayer)

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

//*************************
//	Basics
//*************************

- (CGPoint) pivot { return self.anchorPoint; }
- (void) setPivot:(CGPoint)value
{
	CGRect bounds = [self bounds];
	self.anchorPoint = value;
	self.position = (CGPoint){ bounds.size.width * value.x, bounds.size.height * value.y };
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

@end