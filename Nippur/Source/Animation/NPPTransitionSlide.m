/*
 *	NPPTransitionSlide.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPTransitionSlide.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NPPTransitionSlide

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) executeInView:(UIView *)container fromImg:(UIImage *)fromImg toImg:(UIImage *)toImg
{
	// Definitions.
	CGSize outSize = fromImg.size;
	CGSize inSize = toImg.size;
	CALayer *holderLayer = container.layer;
	
	// From layer (outcoming layer).
	CALayer *outLayer = [CALayer layer];
	outLayer.frame = (CGRect){ CGPointZero, outSize };
	[outLayer setContents:(id)[fromImg CGImage]];
	
	// To layer (incoming layer).
	CALayer *inLayer = [CALayer layer];
	inLayer.frame = (CGRect){ CGPointZero, inSize };
	[inLayer setContents:(id)[toImg CGImage]];
	
	// Fade requires black background.
	container.backgroundColor = [UIColor blackColor];
	
	NSString *keyMove = nil;
	NSString *keyScale = @"transform.scale";
	NSString *keyAlpha = @"opacity";
	CGFloat outMove, inMove;
	CGFloat thirdTime = _duration * 0.33f;
	UIViewAnimationCurve easeIn = UIViewAnimationCurveEaseIn;
	UIViewAnimationCurve easeOut = UIViewAnimationCurveEaseOut;
	
	switch (_direction)
	{
		case NPPDirectionUp:
			keyMove = @"transform.translation.y";
			outMove = -outSize.height;
			inMove = inSize.height;
			break;
		case NPPDirectionDown:
			keyMove = @"transform.translation.y";
			outMove = outSize.height;
			inMove = -inSize.height;
			break;
		case NPPDirectionRight:
			keyMove = @"transform.translation.x";
			outMove = outSize.width;
			inMove = -inSize.width;
			break;
		case NPPDirectionLeft:
		default:
			keyMove = @"transform.translation.x";
			outMove = -outSize.width;
			inMove = inSize.width;
			break;
	}
		
	if (_isBackward)
	{
		[holderLayer addSublayer:inLayer];
		[holderLayer addSublayer:outLayer];
		
		nppAnimateFullTo(outLayer, thirdTime, 0.0f, easeOut, keyScale, 0.75f);
		nppAnimateFullTo(outLayer, thirdTime, 0.0f, easeOut, keyAlpha, 0.5f);
		nppAnimateFullFrom(inLayer, thirdTime, thirdTime, easeIn, keyMove, outMove);
		nppAnimateFullTo(outLayer, thirdTime, thirdTime, easeIn, keyMove, inMove);
		nppAnimateFullFrom(inLayer, thirdTime, thirdTime * 2.0f, easeOut, keyScale, 0.75f);
		nppAnimateFullFrom(inLayer, thirdTime, thirdTime * 2.0f, easeOut, keyAlpha, 0.5f);
	}
	else
	{
		[holderLayer addSublayer:outLayer];
		[holderLayer addSublayer:inLayer];
		
		nppAnimateFullTo(outLayer, thirdTime, 0.0f, easeOut, keyScale, 0.75f);
		nppAnimateFullTo(outLayer, thirdTime, 0.0f, easeOut, keyAlpha, 0.5f);
		nppAnimateFullTo(outLayer, thirdTime, thirdTime, easeIn, keyMove, outMove);
		nppAnimateFullFrom(inLayer, thirdTime, thirdTime, easeIn, keyMove, inMove);
		nppAnimateFullFrom(inLayer, thirdTime, thirdTime * 2.0f, easeOut, keyScale, 0.75f);
		nppAnimateFullFrom(inLayer, thirdTime, thirdTime * 2.0f, easeOut, keyAlpha, 0.5f);
	}
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) setDuration:(CGFloat)duration
{
	// Change the duration, otherwise the callback will be called in the middle of transition.
	[super setDuration:duration * 2.0f];
}

@end

#pragma mark -
#pragma mark NPPTransitionSlideSeguePush
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionSlideSeguePush
//
//**********************************************************************************************************

@implementation NPPTransitionSlideSeguePush

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	UINavigationController *navigation = src.navigationController;
	NPPTransition *transition = [NPPTransitionSlide transitionWithcompletion:nil];
	[navigation nppPushViewController:dest animated:YES transition:transition];
}

@end

#pragma mark -
#pragma mark NPPTransitionSlideSegueModal
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionSlideSegueModal
//
//**********************************************************************************************************

@implementation NPPTransitionSlideSegueModal

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	NPPTransition *transition = [NPPTransitionSlide transitionWithcompletion:nil];
	[src nppPresentViewController:dest animated:YES transition:transition];
}

@end