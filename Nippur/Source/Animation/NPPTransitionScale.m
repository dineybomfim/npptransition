/*
 *	NPPTransitionScale.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPTransitionScale.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NPPTransitionScale

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) executeInView:(UIView *)container fromImg:(UIImage *)fromImg toImg:(UIImage *)toImg
{
	// Definitions.
	CGSize outSize = fromImg.size;
	CGSize inSize = toImg.size;
	CALayer *holderLayer = container.layer;
	
	// From layer (outcoming layer).
	CALayer *outLayer = [CALayer layer];
	outLayer.frame = (CGRect){ CGPointZero, outSize };
	[outLayer setContents:(id)[fromImg CGImage]];
	
	// To layer (incoming layer).
	CALayer *inLayer = [CALayer layer];
	inLayer.frame = (CGRect){ CGPointZero, inSize };
	[inLayer setContents:(id)[toImg CGImage]];
	
	// Fade requires black background.
	container.backgroundColor = [UIColor blackColor];
	
	NSString *keyMove = nil;
	NSString *keyScale = @"transform.scale";
	NSString *keyAlpha = @"opacity";
	CGFloat inMove;
	
	switch (_direction)
	{
		case NPPDirectionUp:
			keyMove = @"transform.translation.y";
			inMove = inSize.height;
			break;
		case NPPDirectionDown:
			keyMove = @"transform.translation.y";
			inMove = -inSize.height;
			break;
		case NPPDirectionRight:
			keyMove = @"transform.translation.x";
			inMove = -inSize.width;
			break;
		case NPPDirectionLeft:
		default:
			keyMove = @"transform.translation.x";
			inMove = inSize.width;
			break;
	}
	
	if (_isBackward)
	{
		[holderLayer addSublayer:inLayer];
		[holderLayer addSublayer:outLayer];
		
		nppAnimateFrom(inLayer, keyScale, 0.75f);
		nppAnimateFrom(inLayer, keyAlpha, 0.5f);
		nppAnimateTo(outLayer, keyMove, inMove);
	}
	else
	{
		[holderLayer addSublayer:outLayer];
		[holderLayer addSublayer:inLayer];
		
		nppAnimateTo(outLayer, keyScale, 0.75f);
		nppAnimateTo(outLayer, keyAlpha, 0.5f);
		nppAnimateFrom(inLayer, keyMove, inMove);
	}
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

@end

#pragma mark -
#pragma mark NPPTransitionScaleSeguePush
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionScaleSeguePush
//
//**********************************************************************************************************

@implementation NPPTransitionScaleSeguePush

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	UINavigationController *navigation = src.navigationController;
	NPPTransition *transition = [NPPTransitionScale transitionWithcompletion:nil];
	[navigation nppPushViewController:dest animated:YES transition:transition];
}

@end

#pragma mark -
#pragma mark NPPTransitionScaleSegueModal
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionScaleSegueModal
//
//**********************************************************************************************************

@implementation NPPTransitionScaleSegueModal

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	NPPTransition *transition = [NPPTransitionScale transitionWithcompletion:nil];
	[src nppPresentViewController:dest animated:YES transition:transition];
}

@end