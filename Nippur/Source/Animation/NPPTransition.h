/*
 *	NPPTransition.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPAnimation.h"

/*!
 *					Defines the direction of the transition.
 *
 *	@var			NPPDirectionLeft
 *					Animation goes to left side of the screen.
 *
 *	@var			NPPDirectionRight
 *					Animation goes to right side of the screen.
 *
 *	@var			NPPDirectionUp
 *					Animation goes to up side of the screen.
 *
 *	@var			NPPDirectionDown
 *					Animation goes to down side of the screen.
 */
typedef enum
{
	NPPDirectionLeft,
	NPPDirectionRight,
	NPPDirectionUp,
	NPPDirectionDown,
} NPPDirection;

/*!
 *					Defines how the transition will manage the views in its superviews.
 *
 *	@var			NPPTransitionModeNavigate
 *					Basic method for View Controllers transition, does nothing in special.
 *					It assumes the minimum size necessary to the animation.
 *
 *	@var			NPPTransitionModeAddRemove
 *					Add and removes the target views from its superviews.
 *					It assumes the maximum size necessary to the animation.
 *					Pay attention to the who really retains a strong reference to the views that is being
 *					animated. They will be retained just during the animation or while the NPPTransition
 *					still alive.
 *
 *	@var			NPPTransitionModeShowHide
 *					Hides and unhides the target views in its superviews.
 *					It assumes the maximum size necessary to the animation.
 *
 *	@var			NPPTransitionModeOverride
 *					This special mode places the destination view (to view) in the same superview as
 *					the initial view (from view).
 *					It assumes the destination view's size to the animation.
 *					Pay attention to the who really retains a strong reference to the views that is being
 *					animated. They will be retained just during the animation or while the NPPTransition
 *					still alive.
 */
typedef enum
{
	NPPTransitionModeNavigate,		// Special - Default
	NPPTransitionModeAddRemove,		// Full
	NPPTransitionModeShowHide,		// Full
	NPPTransitionModeOverride,		// Partial
} NPPTransitionMode;

/*!
 *					Defines type of the animation. It includes the basice types of Cocoa presentations.
 *
 *	@var			NPPTransitionTypePresent
 *					Presents a view as a modal view controller.
 *
 *	@var			NPPTransitionTypeDismiss
 *					Dismiss the current view controller, if there is no current modal, nothing happen.
 *
 *	@var			NPPTransitionTypePush
 *					In navigation controller, it pushes the next view controller.
 *
 *	@var			NPPTransitionTypePop
 *					In navigation controller, it pops the current view controller.
 *
 *	@var			NPPTransitionTypePopTo
 *					In navigation controller, it pops to a specific view controller.
 *
 *	@var			NPPTransitionTypePopToRoot
 *					In navigation controller, it pops to the root view controller.
 */
typedef enum
{
	NPPTransitionTypePresent,		// Default
	NPPTransitionTypeDismiss,
	NPPTransitionTypePush,
	NPPTransitionTypePop,
	NPPTransitionTypePopTo,
	NPPTransitionTypePopToRoot,
} NPPTransitionType;

/*!
 *					Abstract class that defines and makes the transition between two views.
 *					These views can be from view controllers and its hierarchies.
 *					
 *					This class does not actually make the animation, it just defines the required states
 *					and subproducts of the animation. This class must be subclassed to really make an
 *					animation or transition.
 *
 *					The original view (from view) must be a subview of something else, that means,
 *					you can't use it in the top window. It must be made in a subview inside a window.
 */
@interface NPPTransition : NPPAnimation
{
@protected
	UIView						*_fromView;
	UIView						*_toView;
	
	NPPTransitionMode			_mode;
	NPPDirection				_direction;
	BOOL						_isBackward;
	BOOL						_isPreparingBackward;
}

/*!
 *					The original view, that means, the view in which the animation will starts.
 */
@property (nonatomic, NPP_RETAIN) UIView *fromView;

/*!
 *					The target view, that means, the view in which the animation will ends.
 */
@property (nonatomic, NPP_RETAIN) UIView *toView;

/*!
 *					The transition mode. It defines which behavior will be applied to the views
 *					involved in the animation. They can be added, removed, shown, hidden or whatever
 *					other special behavior.
 *
 *	@see			NPPTransitionMode
 */
@property (nonatomic) NPPTransitionMode mode;

/*!
 *					This property defines the direction of the transition. It can be up, right, down
 *					or left.
 *
 *	@see			NPPDirection
 */
@property (nonatomic) NPPDirection direction;

/*!
 *					This property defines the behavior of the animation. The backward animation indicates
 *					that the animation will happen in inverse mode, that means, from the "to view" to the
 *					"from view". Usually this property also indicates that the animation it self will 
 *					happen in the opposite direction.
 */
@property (nonatomic, getter = isBackward) BOOL backward;

/*!
 *					Initializes a new transition from a view, to another view with completion block.
 *					This transition will not start automatically, you must call the #perform# method
 *					to start it.
 *
 *	@param			fromView
 *					The original view in wich the animation will starts from.
 *
 *	@param			toView
 *					The destination view.
 *
 *	@param			block
 *					The completion block, called in the end of the animation.
 *
 *	@result			An initialized instance of this class.
 *
 *	@see			#perform#
 */
- (id) initFromView:(UIView *)fromView toView:(UIView *)toView completion:(NPPBlockVoid)block;

/*!
 *					Creates an autoreleased instance. This method has the same result as calling
 *					#initFromView:toView:completion# method.
 *
 *	@param			fromView
 *					The original view in wich the animation will starts from.
 *
 *	@param			toView
 *					The destination view.
 *
 *	@param			block
 *					The completion block, called in the end of the animation.
 *
 *	@result			An autoreleased instance of this class.
 */
+ (id) transitionFromView:(UIView *)fromView toView:(UIView *)toView completion:(NPPBlockVoid)block;

/*!
 *					Creates an autoreleased instance. This method just sets the completion block.
 *
 *	@param			block
 *					The completion block, called in the end of the animation.
 *
 *	@result			An autoreleased instance of this class.
 */
+ (id) transitionWithcompletion:(NPPBlockVoid)block;

/*!
 *					Immediately performs a backward transition. Calling this method does not affect
 *					the #backward# property.
 *
 *	@see			#backward#
 */
- (void) performBackward;

/*!
 *					This method executes the #performBackward# method after a delay time.
 *
 *	@see			#performBackward#
 */
- (void) performBackwardAfterDelay:(NSTimeInterval)seconds;

/*!
 *					This method immediately cancels any pending perform action with the current instance.
 *
 *	@see			#performBackwardAfterDelay:#
 */
- (void) cancelPerformBackward;

/*!
 *					<strong>(For subclassing only)</strong>
 *					This method is called internally by the transition routine. At this point,
 *					any subclass can override this method to make its own custom animation.
 *
 *					Calling the super it's not mandatory, but could be a good practice to future
 *					subclassing again.
 *
 *	@param			container
 *					The container view in wich the animation will happen.
 *
 *	@param			fromImg
 *					The UIImage that represents the from view. This image has the correct size for
 *					the desired transition.
 *
 *	@param			toImg
 *					The UIImage that represents the to view. This image has the correct size for
 *					the desired transition.
 */
- (void) executeInView:(UIView *)container fromImg:(UIImage *)fromImg toImg:(UIImage *)toImg;

/*!
 *					This method defines the default transition duration.
 *
 *	@param			duration
 *					The duration in seconds.
 */
+ (void) defineTransitionDuration:(CGFloat)duration;

/*!
 *					This method defines the default transition direction. It's not applied to modal and
 *					push transitions.
 *
 *	@param			direction
 *					The direction of the transitions.
 */
+ (void) defineTransitionDirection:(NPPDirection)direction;

/*!
 *					This method defines if the category trick will be used, that means, NPPTransition
 *					can use the "swizzle" trick to change the behavior of the entire application without
 *					any cost to your code.
 *
 *					Setting this property to YES, all the modal routine will be immediately replaced
 *					by the NPPTransition routine. Setting this property to NO will restore the normal
 *					behavior ot UIKit.
 *
 *					By default, this property is set to YES.
 *
 *	@param			useCategory
 *					Indicates if the swizzling will be used or not.
 */
+ (void) defineModalTransitionCategoryUsage:(BOOL)useCategory;

/*!
 *					This method defines the default NPPTransition and direction to be used with
 *					modal transitions.
 *
 *					By default, it's set to NPPTransitionCube and NPPDirectionUp.
 *
 *	@param			aClass
 *					The default class to be used with modal transitions.
 *
 *	@param			direction
 *					The default direction to be used with modal transitions.
 */
+ (void) defineModalTransitionClass:(Class)aClass direction:(NPPDirection)direction;

/*!
 *					This method defines if the category trick will be used, that means, NPPTransition
 *					can use the "swizzle" trick to change the behavior of the entire application without
 *					any cost to your code.
 *
 *					Setting this property to YES, all the push routine will be immediately replaced
 *					by the NPPTransition routine. Setting this property to NO will restore the normal
 *					behavior ot UIKit.
 *
 *					By default, this property is set to YES.
 *
 *	@param			useCategory
 *					Indicates if the swizzling will be used or not.
 */
+ (void) definePushTransitionCategoryUsage:(BOOL)useCategory;

/*!
 *					This method defines the default NPPTransition and direction to be used with
 *					push transitions.
 *
 *					By default, it's set to NPPTransitionScale and NPPDirectionLeft.
 *
 *	@param			aClass
 *					The default class to be used with push transitions.
 *
 *	@param			direction
 *					The default direction to be used with push transitions.
 */
+ (void) definePushTransitionClass:(Class)aClass direction:(NPPDirection)direction;

@end

/*!
 *					This category implements the basic methods to make NPPTransition with modal behavior.
 */
@interface UIViewController(NPPTransition)

/*!
 *					Makes the presentation of a modal. Behaves exactly like the normal
 *					presentViewController, however with a NPPTransition property.
 *
 *					If the NPPTransition is nil, the default class for modal NPPTransition will be
 *					used in place.
 *
 *	@param			controller
 *					The new controller to present.
 *
 *	@param			flag
 *					Indicates if the transition will be animated or not.
 *
 *	@param			transition
 *					A custom NPPTransition. The properties #fromView#, #toView# and #backward# will
 *					be ignored from the original transition. If this parameter is nil, the default
 *					NPPTransition for modal will be used.
 */
- (void) nppPresentViewController:(UIViewController *)controller
						 animated:(BOOL)flag
					   transition:(NPPTransition *)transition;

/*!
 *					Makes the dismiss of a modal. Behaves exactly like the normal
 *					dismissViewController, however with a NPPTransition property.
 *
 *					If the NPPTransition is nil, the default class for modal NPPTransition will be
 *					used in place.
 *
 *	@param			flag
 *					Indicates if the transition will be animated or not.
 *
 *	@param			transition
 *					A custom NPPTransition. The properties #fromView#, #toView# and #backward# will
 *					be ignored from the original transition. If this parameter is nil, the default
 *					NPPTransition for modal will be used.
 */
- (void) nppDismissViewControllerAnimated:(BOOL)flag transition:(NPPTransition *)transition;

@end

/*!
 *					This category implements the basic methods to make NPPTransition with push behavior.
 */
@interface UINavigationController(NPPTransition)

/*!
 *					Makes the push of a navigation controller. Behaves exactly like the normal
 *					pushViewController, however with a NPPTransition property.
 *
 *					If the NPPTransition is nil, the default class for push NPPTransition will be
 *					used in place.
 *
 *	@param			controller
 *					The new controller to present.
 *
 *	@param			flag
 *					Indicates if the transition will be animated or not.
 *
 *	@param			transition
 *					A custom NPPTransition. The properties #fromView#, #toView# and #backward# will
 *					be ignored from the original transition. If this parameter is nil, the default
 *					NPPTransition for push will be used.
 */
- (void) nppPushViewController:(UIViewController *)controller
					  animated:(BOOL)flag
					transition:(NPPTransition *)transition;

/*!
 *					Makes the pop of the current view controller. Behaves exactly like the normal
 *					popViewController, however with a NPPTransition property.
 *
 *					If the NPPTransition is nil, the default class for push NPPTransition will be
 *					used in place.
 *
 *	@param			flag
 *					Indicates if the transition will be animated or not.
 *
 *	@param			transition
 *					A custom NPPTransition. The properties #fromView#, #toView# and #backward# will
 *					be ignored from the original transition. If this parameter is nil, the default
 *					NPPTransition for push will be used.
 */
- (UIViewController *) nppPopViewControllerAnimated:(BOOL)flag transition:(NPPTransition *)transition;

/*!
 *					Makes the pop to the root view controller. Behaves exactly like the normal
 *					popToRootViewController, however with a NPPTransition property.
 *
 *					If the NPPTransition is nil, the default class for push NPPTransition will be
 *					used in place.
 *
 *	@param			flag
 *					Indicates if the transition will be animated or not.
 *
 *	@param			transition
 *					A custom NPPTransition. The properties #fromView#, #toView# and #backward# will
 *					be ignored from the original transition. If this parameter is nil, the default
 *					NPPTransition for push will be used.
 */
- (NSArray *) nppPopToRootViewControllerAnimated:(BOOL)flag transition:(NPPTransition *)transition;

/*!
 *					Makes the pop to a specific view controller. Behaves exactly like the normal
 *					popToViewController, however with a NPPTransition property.
 *
 *					If the NPPTransition is nil, the default class for push NPPTransition will be
 *					used in place.
 *
 *	@param			controller
 *					The view controller that you want to be at the top of the stack.
 *
 *	@param			flag
 *					Indicates if the transition will be animated or not.
 *
 *	@param			transition
 *					A custom NPPTransition. The properties #fromView#, #toView# and #backward# will
 *					be ignored from the original transition. If this parameter is nil, the default
 *					NPPTransition for push will be used.
 */
- (NSArray *) nppPopToViewController:(UIViewController *)controller
							animated:(BOOL)flag
						  transition:(NPPTransition *)transition;

@end