/*
 *	NPPTransitionFlip.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPTransitionFlip.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NPPTransitionFlip

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) executeInView:(UIView *)container fromImg:(UIImage *)fromImg toImg:(UIImage *)toImg
{
	// Definitions.
	CGSize outSize = fromImg.size;
	CGSize inSize = toImg.size;
	CALayer *holderLayer = container.layer;
	
	// From layer (outcoming layer).
	CALayer *outLayer = [CALayer layer];
	outLayer.frame = (CGRect){ CGPointZero, outSize };
	[outLayer setContents:(id)[fromImg CGImage]];
	
	// To layer (incoming layer).
	CALayer *inLayer = [CALayer layer];
	inLayer.frame = (CGRect){ CGPointZero, inSize };
	[inLayer setContents:(id)[toImg CGImage]];
	
	// Fade requires black background.
	container.backgroundColor = [UIColor blackColor];
	
	NSString *keyRotation = nil;
	NSString *keyScale = @"transform.scale";
	NSString *keyAlpha = @"opacity";
	CGFloat outRotation, inRotation;
	CGFloat time = _duration * 0.5f;
	UIViewAnimationCurve easeIn = UIViewAnimationCurveEaseIn;
	UIViewAnimationCurve easeOut = UIViewAnimationCurveEaseOut;
	
	// Core Animation perspective camera simulation.
	CATransform3D transform = CATransform3DIdentity;
	transform.m34 = -1.0 / (outSize.height * 2.0f);
	container.layer.sublayerTransform = transform;
	
	switch (_direction)
	{
		case NPPDirectionUp:
			keyRotation = @"transform.rotation.x";
			outRotation = 90.0f;
			inRotation = -90.0f;
			break;
		case NPPDirectionDown:
			keyRotation = @"transform.rotation.x";
			outRotation = -90.0f;
			inRotation = 90.0f;
			break;
		case NPPDirectionRight:
			keyRotation = @"transform.rotation.y";
			outRotation = 90.0f;
			inRotation = -90.0f;
			break;
		case NPPDirectionLeft:
		default:
			keyRotation = @"transform.rotation.y";
			outRotation = -90.0f;
			inRotation = 90.0f;
			break;
	}
	
	outRotation = NPPDegreesToRadians(outRotation);
	inRotation = NPPDegreesToRadians(inRotation);
	
	if (_isBackward)
	{
		[holderLayer addSublayer:inLayer];
		[holderLayer addSublayer:outLayer];
		
		nppAnimateFullFrom(inLayer, time, time, easeOut, keyScale, 0.75f);
		nppAnimateFullFrom(inLayer, time, time, easeOut, keyAlpha, 0.5f);
		nppAnimateFullFrom(inLayer, time, time, easeOut, keyRotation, outRotation);
		nppAnimateFullTo(outLayer, time, 0.0f, easeIn, keyScale, 0.75f);
		nppAnimateFullTo(outLayer, time, 0.0f, easeIn, keyAlpha, 0.5f);
		nppAnimateFullTo(outLayer, time, 0.0f, easeIn, keyRotation, inRotation);
	}
	else
	{
		[holderLayer addSublayer:outLayer];
		[holderLayer addSublayer:inLayer];
		
		nppAnimateFullTo(outLayer, time, 0.0f, easeIn, keyScale, 0.75f);
		nppAnimateFullTo(outLayer, time, 0.0f, easeIn, keyAlpha, 0.5f);
		nppAnimateFullTo(outLayer, time, 0.0f, easeIn, keyRotation, outRotation);
		nppAnimateFullFrom(inLayer, time, time, easeOut, keyScale, 0.75f);
		nppAnimateFullFrom(inLayer, time, time, easeOut, keyAlpha, 0.5f);
		nppAnimateFullFrom(inLayer, time, time, easeOut, keyRotation, inRotation);
	}
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) setDuration:(CGFloat)duration
{
	// Change the duration, otherwise the callback will be called in the middle of transition.
	[super setDuration:duration * 2.0f];
}

@end

#pragma mark -
#pragma mark NPPTransitionFlipSeguePush
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionFlipSeguePush
//
//**********************************************************************************************************

@implementation NPPTransitionFlipSeguePush

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	UINavigationController *navigation = src.navigationController;
	NPPTransition *transition = [NPPTransitionFlip transitionWithcompletion:nil];
	[navigation nppPushViewController:dest animated:YES transition:transition];
}

@end

#pragma mark -
#pragma mark NPPTransitionFlipSegueModal
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionFlipSegueModal
//
//**********************************************************************************************************

@implementation NPPTransitionFlipSegueModal

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	NPPTransition *transition = [NPPTransitionFlip transitionWithcompletion:nil];
	[src nppPresentViewController:dest animated:YES transition:transition];
}

@end