/*
 *	NPPTransitionCube.m
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPTransitionCube.h"

#pragma mark -
#pragma mark Constants
#pragma mark -
//**********************************************************************************************************
//
//	Constants
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Interface
#pragma mark -
//**********************************************************************************************************
//
//	Private Interface
//
//**********************************************************************************************************

#pragma mark -
#pragma mark Private Definitions
//**************************************************
//	Private Definitions
//**************************************************

#pragma mark -
#pragma mark Private Functions
//**************************************************
//	Private Functions
//**************************************************

#pragma mark -
#pragma mark Private Category
//**************************************************
//	Private Category
//**************************************************

#pragma mark -
#pragma mark Public Interface
#pragma mark -
//**********************************************************************************************************
//
//	Public Interface
//
//**********************************************************************************************************

@implementation NPPTransitionCube

#pragma mark -
#pragma mark Properties
//**************************************************
//	Properties
//**************************************************

#pragma mark -
#pragma mark Constructors
//**************************************************
//	Constructors
//**************************************************

#pragma mark -
#pragma mark Private Methods
//**************************************************
//	Private Methods
//**************************************************

#pragma mark -
#pragma mark Self Public Methods
//**************************************************
//	Self Public Methods
//**************************************************

- (void) executeInView:(UIView *)container fromImg:(UIImage *)fromImg toImg:(UIImage *)toImg
{
	// Definitions.
	CGSize outSize = fromImg.size;
	CGSize inSize = toImg.size;
	CALayer *holderLayer = container.layer;
	
	// Colors.
	UIColor *blackColor = [UIColor blackColor];
	UIColor *clearColor = [UIColor clearColor];
	
	// From layer (outcoming layer).
	CALayer *outLayer = [CALayer layer];
	outLayer.frame = (CGRect){ CGPointZero, outSize };
	[outLayer setContents:(id)[fromImg CGImage]];
	
	// To layer (incoming layer).
	CALayer *inLayer = [CALayer layer];
	inLayer.frame = (CGRect){ CGPointZero, inSize };
	[inLayer setContents:(id)[toImg CGImage]];
	
	// Shadow layer.
	CAGradientLayer *outShadow = [CAGradientLayer layer];
	outShadow.frame = outLayer.frame;
	outShadow.opacity = 0.75f;
	outShadow.colors = [NSArray arrayWithObjects:(id)[blackColor CGColor], (id)[clearColor CGColor], nil];
	
	CAGradientLayer *inShadow = [CAGradientLayer layer];
	inShadow.frame = inLayer.frame;
	inShadow.opacity = 0.75f;
	inShadow.colors = [NSArray arrayWithObjects:(id)[blackColor CGColor], (id)[clearColor CGColor], nil];
	
	// Fade requires black background.
	container.backgroundColor = blackColor;
	
	NSString *keyMove = nil;
	NSString *keyRotation = nil;
	NSString *keyAlpha = @"opacity";
	NSString *keyScale = @"transform.scale";
	CGFloat outRotation, inRotation;
	CGPoint outPivot, inPivot;
	CGPoint startPoint, endPoint;
	CGFloat outMove, inMove;
	CGFloat halfTime = _duration * 0.5f;
	UIViewAnimationCurve easeLinear = UIViewAnimationCurveLinear;
	
	// Core Animation perspective camera simulation.
	CATransform3D transform = CATransform3DIdentity;
	transform.m34 = -1.0 / (MAX(outSize.width, outSize.height) * 1.0f);
	container.layer.sublayerTransform = transform;
	
	switch (_direction)
	{
		case NPPDirectionUp:
			keyMove = @"transform.translation.y";
			keyRotation = @"transform.rotation.x";
			outRotation = 90.0f;
			inRotation = -90.0f;
			outMove = -outSize.height;
			inMove = inSize.height;
			outPivot = (CGPoint){ 0.5f, 1.0f };
			inPivot = (CGPoint){ 0.5f, 0.0f };
			startPoint = (CGPoint){ 0.5f, 0.0f };
			endPoint = (CGPoint){ 0.5f, 1.0f };
			break;
		case NPPDirectionDown:
			keyMove = @"transform.translation.y";
			keyRotation = @"transform.rotation.x";
			outRotation = -90.0f;
			inRotation = 90.0f;
			outMove = outSize.height;
			inMove = -inSize.height;
			outPivot = (CGPoint){ 0.5f, 0.0f };
			inPivot = (CGPoint){ 0.5f, 1.0f };
			startPoint = (CGPoint){ 0.5f, 0.0f };
			endPoint = (CGPoint){ 0.5f, 1.0f };
			break;
		case NPPDirectionRight:
			keyMove = @"transform.translation.x";
			keyRotation = @"transform.rotation.y";
			outRotation = 90.0f;
			inRotation = -90.0f;
			outMove = outSize.width;
			inMove = -inSize.width;
			outPivot = (CGPoint){ 0.0f, 0.5f };
			inPivot = (CGPoint){ 1.0f, 0.5f };
			startPoint = (CGPoint){ 0.0f, 0.5f };
			endPoint = (CGPoint){ 1.0f, 0.5f };
			break;
		case NPPDirectionLeft:
		default:
			keyMove = @"transform.translation.x";
			keyRotation = @"transform.rotation.y";
			outRotation = -90.0f;
			inRotation = 90.0f;
			outMove = -outSize.width;
			inMove = inSize.width;
			outPivot = (CGPoint){ 1.0f, 0.5f };
			inPivot = (CGPoint){ 0.0f, 0.5f };
			startPoint = (CGPoint){ 0.0f, 0.5f };
			endPoint = (CGPoint){ 1.0f, 0.5f };
			break;
	}
	
	outRotation = NPPDegreesToRadians(outRotation);
	inRotation = NPPDegreesToRadians(inRotation);
	outShadow.startPoint = startPoint;
	outShadow.endPoint = endPoint;
	inShadow.startPoint = startPoint;
	inShadow.endPoint = endPoint;
	outShadow.frame = CGRectMake(0.0f, 0.0f, outSize.width * startPoint.y, outSize.height * startPoint.x);
	inShadow.frame = CGRectMake(0.0f, 0.0f, inSize.width * startPoint.y, inSize.height * startPoint.x);
	
	if (_isBackward)
	{
		[holderLayer addSublayer:inLayer];
		[holderLayer addSublayer:outLayer];
		[inLayer addSublayer:outShadow];
		[outLayer addSublayer:inShadow];
		
		outLayer.pivot = inPivot;
		inLayer.pivot = outPivot;
		
		nppAnimateFrom(inLayer, keyAlpha, 0.5f);
		nppAnimateFrom(inLayer, keyRotation, outRotation);
		nppAnimateFrom(inLayer, keyMove, outMove);
		nppAnimateFullTo(inLayer, halfTime, 0.0f, easeLinear, keyScale, 0.75f);
		nppAnimateFullTo(inLayer, halfTime, halfTime, easeLinear, keyScale, 1.0f);
		
		nppAnimateTo(outLayer, keyAlpha, 0.5f);
		nppAnimateTo(outLayer, keyRotation, inRotation);
		nppAnimateTo(outLayer, keyMove, inMove);
		nppAnimateFullTo(outLayer, halfTime, 0.0f, easeLinear, keyScale, 0.75f);
		nppAnimateFullTo(outLayer, halfTime, halfTime, easeLinear, keyScale, 1.0f);
		
		nppAnimateTo(outShadow, keyAlpha, 0.0f);
		nppAnimateFrom(inShadow, keyAlpha, 0.0f);
	}
	else
	{
		[holderLayer addSublayer:outLayer];
		[holderLayer addSublayer:inLayer];
		[inLayer addSublayer:inShadow];
		[outLayer addSublayer:outShadow];
		
		outLayer.pivot = outPivot;
		inLayer.pivot = inPivot;
		
		nppAnimateTo(outLayer, keyAlpha, 0.5f);
		nppAnimateTo(outLayer, keyRotation, outRotation);
		nppAnimateTo(outLayer, keyMove, outMove);
		nppAnimateFullTo(outLayer, halfTime, 0.0f, easeLinear, keyScale, 0.75f);
		nppAnimateFullTo(outLayer, halfTime, halfTime, easeLinear, keyScale, 1.0f);
		
		nppAnimateFrom(inLayer, keyAlpha, 0.5f);
		nppAnimateFrom(inLayer, keyRotation, inRotation);
		nppAnimateFrom(inLayer, keyMove, inMove);
		nppAnimateFullTo(inLayer, halfTime, 0.0f, easeLinear, keyScale, 0.75f);
		nppAnimateFullTo(inLayer, halfTime, halfTime, easeLinear, keyScale, 1.0f);
		
		nppAnimateFrom(outShadow, keyAlpha, 0.0f);
		nppAnimateTo(inShadow, keyAlpha, 0.0f);
	}
}

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) setDuration:(CGFloat)duration
{
	// Change the duration, otherwise the callback will be called in the middle of transition.
	[super setDuration:duration * 2.0f];
}

@end

#pragma mark -
#pragma mark NPPTransitionCubeSeguePush
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionCubeSeguePush
//
//**********************************************************************************************************

@implementation NPPTransitionCubeSeguePush

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	UINavigationController *navigation = src.navigationController;
	NPPTransition *transition = [NPPTransitionCube transitionWithcompletion:nil];
	[navigation nppPushViewController:dest animated:YES transition:transition];
}

@end

#pragma mark -
#pragma mark NPPTransitionCubeSegueModal
#pragma mark -
//**********************************************************************************************************
//
//	NPPTransitionCubeSegueModal
//
//**********************************************************************************************************

@implementation NPPTransitionCubeSegueModal

#pragma mark -
#pragma mark Override Public Methods
//**************************************************
//	Override Public Methods
//**************************************************

- (void) perform
{
    UIViewController *src = self.sourceViewController;
    UIViewController *dest = self.destinationViewController;
	NPPTransition *transition = [NPPTransitionCube transitionWithcompletion:nil];
	[src nppPresentViewController:dest animated:YES transition:transition];
}

@end