/*
 *	NPPTransitionScale.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPTransition.h"

/*!
 *					Scale transition for NPPTransition.
 */
@interface NPPTransitionScale : NPPTransition

@end

/*!
 *					Segue for push scale transition for NPPTransition.
 */
@interface NPPTransitionScaleSeguePush : UIStoryboardSegue

@end

/*!
 *					Segue for modal scale transition for NPPTransition.
 */
@interface NPPTransitionScaleSegueModal : UIStoryboardSegue

@end