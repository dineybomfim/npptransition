/*
 *	NPPAnimation.h
 *	Nippur
 *
 *	The Nippur is a framework for iOS to make daily work easier and more reusable.
 *	This Nippur contains many API and includes many wrappers to other Apple frameworks.
 *
 *	More information at the official web site: http://db-in.com/nippur
 *	
 *	Created by Diney Bomfim on 6/1/13.
 *	Copyright 2013 db-in. All rights reserved.
 */

#import "NPPRuntime.h"
#import "NPPFunctions.h"
#import "NPPView+UIView.h"

/*!
 *					This function creates a CABasicAnimation based on a target, keypath and toValue.
 *					It automatically use the fillMode to kCAFillModeForwards and removedOnCompletion to NO.
 *
 *	@param			target
 *					The object that will be animated.
 *
 *	@param			keypath
 *					The keypath to the property that will be animated.
 *
 *	@param			toValue
 *					The final property's value.
 */
extern void nppAnimateTo(CALayer *target, NSString *keyPath, CGFloat toValue);

/*!
 *					This function creates a CABasicAnimation based on a target, keypath and fromValue.
 *					It automatically use the fillMode to kCAFillModeForwards and removedOnCompletion to NO.
 *
 *	@param			target
 *					The object that will be animated.
 *
 *	@param			keypath
 *					The keypath to the property that will be animated.
 *
 *	@param			fromValue
 *					The initial property's value. The final value will be the current value.
 */
extern void nppAnimateFrom(CALayer *target, NSString *keyPath, CGFloat fromValue);

/*!
 *					Similar to #nppAnimateTo# however this function offers more settings, like
 *					duration, delay and ease.
 *					It automatically use the fillMode to kCAFillModeForwards and removedOnCompletion to NO.
 *
 *	@param			target
 *					The object that will be animated.
 *
 *	@param			duration
 *					The animation's duration.
 *
 *	@param			delay
 *					The delay in which the animation should occur.
 *
 *	@param			ease
 *					The ease function that will guide the animation.
 *
 *	@param			keypath
 *					The keypath to the property that will be animated.
 *
 *	@param			toValue
 *					The final property's value.
 */
extern void nppAnimateFullTo(CALayer *target,
							 CGFloat duration,
							 CGFloat delay,
							 UIViewAnimationCurve ease,
							 NSString *keyPath,
							 CGFloat toValue);

/*!
 *					Similar to #nppAnimateFrom# however this function offers more settings, like
 *					duration, delay and ease.
 *					It automatically use the fillMode to kCAFillModeForwards and removedOnCompletion to NO.
 *
 *	@param			target
 *					The object that will be animated.
 *
 *	@param			duration
 *					The animation's duration.
 *
 *	@param			delay
 *					The delay in which the animation should occur.
 *
 *	@param			ease
 *					The ease function that will guide the animation.
 *
 *	@param			keypath
 *					The keypath to the property that will be animated.
 *
 *	@param			fromValue
 *					The initial property's value. The final value will be the current value.
 */
extern void nppAnimateFullFrom(CALayer *target,
							   CGFloat duration,
							   CGFloat delay,
							   UIViewAnimationCurve ease,
							   NSString *keyPath,
							   CGFloat fromValue);

/*!
 *					Basic abstract class that defines the animation behavior.
 *					This class will does nothing by it self, it must be subclassed.
 */
@interface NPPAnimation : NSObject
{
@protected
	BOOL						_isAnimating;
	CGFloat						_duration;
	NPPBlockVoid				_completionBlock;
}

/*!
 *					The animation's duration in seconds.
 */
@property (nonatomic) CGFloat duration;

/*!
 *					The completion block. This block will be called in the end of the animation.
 */
@property (nonatomic, NPP_COPY) NPPBlockVoid completionBlock;

/*!
 *					Initializes a new animation object with a duration, in seconds, and a completion block.
 *
 *	@param			duration
 *					The animation's duration in seconds.
 *
 *	@param			block
 *					This C block will be called in the end of the animation.
 *
 *	@result			An initialized instance of this class.
 */
- (id) initWithDuration:(CGFloat)duration completion:(NPPBlockVoid)block;

/*!
 *					This is the main method to perform an animation. It must be subclassed.
 */
- (void) perform;

@end